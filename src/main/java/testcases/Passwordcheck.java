package testcases;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Passwordcheck {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//requirement is at least 4 small letters and 4 numbers and one special character 
		
		String str = "testABCD1234@";
		
		System.out.println(Pattern.matches("[a-z]{4}[A-Z]{4}[0-9]{4}[\\W]{1}", str ));
		
		//Output only digits which are dynamic
		
		String str2 = "test123456abc";
		
		String delimiter = "[a-zA-Z]";
		
		Pattern pattern = Pattern.compile(delimiter);
		
		String[] split = pattern.split(str2);
		
		for (String string1 : split) {
			
			System.out.println(string1);
			
		}
		
		
		//verify if the mobile no's are 10 digits and starts with 7 or 8 or 9
		
		String[] str1 = {"9871425632", "7425612541", "7412358965"};
		
		for (String string : str1) {
			
			System.out.println(Pattern.matches("[789][0-9]{9}", string));
			
		}
		
		//Verify the position of first no in the given string
		
		String str3 = "test12345ing";
		
		String delimiter1 = "[0-9]{5}";
		
		Pattern pattern2 = pattern.compile(delimiter1);
		
		Matcher matcher = pattern2.matcher(str3);
		
		
		while(matcher.find()){
		System.out.println("Pattern found from " + matcher.start() + " to " + (matcher.end()-1));
			
		}
		
		
		

	}

}
