package testcases;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC001_HITS_Login_Validation extends ProjectMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC001_HITS_Login_Validation";
		testCaseDescription ="Validation of login to HITS Application post patching is successful";
		category = "Smoke";
		author= "Vinod";
		
	}
	
	@Test
	public  void loginHITS() throws InterruptedException   {
		
		WebElement title = locateElement("xpath", "//td[@class='title']");
		String actualTitle = title.getText();
		System.out.println(actualTitle);
		
		
		String expectedTitle = "  MAPMG Hospitalist & Inpatient Tracking System";
		
		
		if(actualTitle.equalsIgnoreCase(expectedTitle)){
			
			System.out.println("Title Matched");
		}
		else System.out.println("Title Not Matched");
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snaps_HITS/Login.png");
		try {
			FileUtils.copyFile(src, dest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Thread.sleep(3000);
		
		
		WebElement findPatients = locateElement("xpath", "//a[@href='findPatients.aspx']");
		findPatients.click();
		
		
	}
	

}
