package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Parameters;



public class ProjectMethods extends SeMethods{

	
	@Parameters({"url","uname","pwd"})
	@BeforeMethod
	public void login(String url, String userName, String passWord) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, userName);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, passWord);
		WebElement eleSignon = locateElement("xpath","//span[text() = 'Sign on']");
		click(eleSignon);
	}
	
	@AfterMethod
	public void close() {
		closeAllBrowsers();
	}
	
	
	
	
	
	
	
	
	
	
	
}